from conan import ConanFile

class IDS3010Recipe(ConanFile):
    name = "ids3010"
    executable = "ds_IDS3010"
    version = "2.2.0"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Florent Langlois"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/measureinstruments/attocube/ids3010.git"
    description = "IDS3010 device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("idslibrary/[>=1.0]@soleil/stable")
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
