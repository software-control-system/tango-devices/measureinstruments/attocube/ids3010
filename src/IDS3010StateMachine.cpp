/*----- PROTECTED REGION ID(IDS3010StateMachine.cpp) ENABLED START -----*/
static const char *RcsId = "$Id:  $";
//=============================================================================
//
// file :        IDS3010StateMachine.cpp
//
// description : State machine file for the IDS3010 class
//
// project :     IDS 3010 attocube interferometer
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include <IDS3010.h>

/*----- PROTECTED REGION END -----*/	//	IDS3010::IDS3010StateMachine.cpp

//================================================================
//  States   |  Description
//================================================================
//  FAULT    |  IDS is in error
//  STANDBY  |  IDS is waiting for request
//  RUNNING  |  IDS is Measuring
//  MOVING   |  IDS is opticaly aligning
//  DISABLE  |  IDS is starting a measurement or an optics alignement
//  ON       |  Pilot laser is Enabled


namespace IDS3010_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_axis1AbsPosition_allowed()
 *	Description : Execution allowed for axis1AbsPosition attribute
 */
//--------------------------------------------------------
bool IDS3010::is_axis1AbsPosition_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::MOVING ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::axis1AbsPositionStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::axis1AbsPositionStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_axis2AbsPosition_allowed()
 *	Description : Execution allowed for axis2AbsPosition attribute
 */
//--------------------------------------------------------
bool IDS3010::is_axis2AbsPosition_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::MOVING ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::axis2AbsPositionStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::axis2AbsPositionStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_axis3AbsPosition_allowed()
 *	Description : Execution allowed for axis3AbsPosition attribute
 */
//--------------------------------------------------------
bool IDS3010::is_axis3AbsPosition_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::MOVING ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::axis3AbsPositionStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::axis3AbsPositionStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_axis1Displacement_allowed()
 *	Description : Execution allowed for axis1Displacement attribute
 */
//--------------------------------------------------------
bool IDS3010::is_axis1Displacement_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::MOVING ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::axis1DisplacementStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::axis1DisplacementStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_axis2Displacement_allowed()
 *	Description : Execution allowed for axis2Displacement attribute
 */
//--------------------------------------------------------
bool IDS3010::is_axis2Displacement_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::MOVING ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::axis2DisplacementStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::axis2DisplacementStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_axis3Displacement_allowed()
 *	Description : Execution allowed for axis3Displacement attribute
 */
//--------------------------------------------------------
bool IDS3010::is_axis3Displacement_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::MOVING ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::axis3DisplacementStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::axis3DisplacementStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_axis1Contrast_allowed()
 *	Description : Execution allowed for axis1Contrast attribute
 */
//--------------------------------------------------------
bool IDS3010::is_axis1Contrast_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::axis1ContrastStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::axis1ContrastStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_axis2Contrast_allowed()
 *	Description : Execution allowed for axis2Contrast attribute
 */
//--------------------------------------------------------
bool IDS3010::is_axis2Contrast_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::axis2ContrastStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::axis2ContrastStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_axis3Contrast_allowed()
 *	Description : Execution allowed for axis3Contrast attribute
 */
//--------------------------------------------------------
bool IDS3010::is_axis3Contrast_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::axis3ContrastStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::axis3ContrastStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_pilotLaser_allowed()
 *	Description : Execution allowed for pilotLaser attribute
 */
//--------------------------------------------------------
bool IDS3010::is_pilotLaser_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING ||
			get_state()==Tango::MOVING ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::pilotLaserStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::pilotLaserStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_passModeStr_allowed()
 *	Description : Execution allowed for passModeStr attribute
 */
//--------------------------------------------------------
bool IDS3010::is_passModeStr_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::passModeStrStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	IDS3010::passModeStrStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_passMode_allowed()
 *	Description : Execution allowed for passMode attribute
 */
//--------------------------------------------------------
bool IDS3010::is_passMode_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING ||
			get_state()==Tango::MOVING ||
			get_state()==Tango::DISABLE ||
			get_state()==Tango::ON)
		{
		/*----- PROTECTED REGION ID(IDS3010::passModeStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	IDS3010::passModeStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::passModeStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	IDS3010::passModeStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_passModeEnum_allowed()
 *	Description : Execution allowed for passModeEnum attribute
 */
//--------------------------------------------------------
bool IDS3010::is_passModeEnum_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING ||
			get_state()==Tango::MOVING ||
			get_state()==Tango::DISABLE ||
			get_state()==Tango::ON)
		{
		/*----- PROTECTED REGION ID(IDS3010::passModeEnumStateAllowed_WRITE) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::passModeEnumStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::passModeEnumStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::passModeEnumStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_averageN_allowed()
 *	Description : Execution allowed for averageN attribute
 */
//--------------------------------------------------------
bool IDS3010::is_averageN_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING ||
			get_state()==Tango::MOVING ||
			get_state()==Tango::DISABLE ||
			get_state()==Tango::ON)
		{
		/*----- PROTECTED REGION ID(IDS3010::averageNStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	IDS3010::averageNStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::averageNStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::averageNStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_baseBandAmplitude_allowed()
 *	Description : Execution allowed for baseBandAmplitude attribute
 */
//--------------------------------------------------------
bool IDS3010::is_baseBandAmplitude_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::baseBandAmplitudeStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::baseBandAmplitudeStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_periodSinCosClk_allowed()
 *	Description : Execution allowed for periodSinCosClk attribute
 */
//--------------------------------------------------------
bool IDS3010::is_periodSinCosClk_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING ||
			get_state()==Tango::MOVING ||
			get_state()==Tango::DISABLE ||
			get_state()==Tango::ON)
		{
		/*----- PROTECTED REGION ID(IDS3010::periodSinCosClkStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	IDS3010::periodSinCosClkStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::periodSinCosClkStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::periodSinCosClkStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_resolutionSinCos_allowed()
 *	Description : Execution allowed for resolutionSinCos attribute
 */
//--------------------------------------------------------
bool IDS3010::is_resolutionSinCos_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING ||
			get_state()==Tango::MOVING ||
			get_state()==Tango::DISABLE ||
			get_state()==Tango::ON)
		{
		/*----- PROTECTED REGION ID(IDS3010::resolutionSinCosStateAllowed_WRITE) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::resolutionSinCosStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(IDS3010::resolutionSinCosStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::resolutionSinCosStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_rtOutModeStr_allowed()
 *	Description : Execution allowed for rtOutModeStr attribute
 */
//--------------------------------------------------------
bool IDS3010::is_rtOutModeStr_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::rtOutModeStrStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::rtOutModeStrStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_rtOutMode_allowed()
 *	Description : Execution allowed for rtOutMode attribute
 */
//--------------------------------------------------------
bool IDS3010::is_rtOutMode_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::RUNNING ||
			get_state()==Tango::MOVING ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::rtOutModeStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	IDS3010::rtOutModeStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::DISABLE)
		{
		/*----- PROTECTED REGION ID(IDS3010::rtOutModeStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	IDS3010::rtOutModeStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_softwareVersion_allowed()
 *	Description : Execution allowed for softwareVersion attribute
 */
//--------------------------------------------------------
bool IDS3010::is_softwareVersion_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(IDS3010::softwareVersionStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	IDS3010::softwareVersionStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_fpgaVersion_allowed()
 *	Description : Execution allowed for fpgaVersion attribute
 */
//--------------------------------------------------------
bool IDS3010::is_fpgaVersion_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(IDS3010::fpgaVersionStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	IDS3010::fpgaVersionStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_firmwareVersion_allowed()
 *	Description : Execution allowed for firmwareVersion attribute
 */
//--------------------------------------------------------
bool IDS3010::is_firmwareVersion_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for firmwareVersion attribute in read access.
	/*----- PROTECTED REGION ID(IDS3010::firmwareVersionStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	IDS3010::firmwareVersionStateAllowed_READ
	return true;
}


//=================================================
//		Commands Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_StartMeasurement_allowed()
 *	Description : Execution allowed for StartMeasurement attribute
 */
//--------------------------------------------------------
bool IDS3010::is_StartMeasurement_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT ||
		get_state()==Tango::RUNNING ||
		get_state()==Tango::MOVING ||
		get_state()==Tango::DISABLE ||
		get_state()==Tango::ON)
	{
	/*----- PROTECTED REGION ID(IDS3010::StartMeasurementStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	IDS3010::StartMeasurementStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_StopMeasurement_allowed()
 *	Description : Execution allowed for StopMeasurement attribute
 */
//--------------------------------------------------------
bool IDS3010::is_StopMeasurement_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT ||
		get_state()==Tango::MOVING ||
		get_state()==Tango::DISABLE ||
		get_state()==Tango::ON)
	{
	/*----- PROTECTED REGION ID(IDS3010::StopMeasurementStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	IDS3010::StopMeasurementStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_StartOpticsAlignment_allowed()
 *	Description : Execution allowed for StartOpticsAlignment attribute
 */
//--------------------------------------------------------
bool IDS3010::is_StartOpticsAlignment_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT ||
		get_state()==Tango::RUNNING ||
		get_state()==Tango::MOVING ||
		get_state()==Tango::DISABLE ||
		get_state()==Tango::ON)
	{
	/*----- PROTECTED REGION ID(IDS3010::StartOpticsAlignmentStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	IDS3010::StartOpticsAlignmentStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_StopOpticsAlignment_allowed()
 *	Description : Execution allowed for StopOpticsAlignment attribute
 */
//--------------------------------------------------------
bool IDS3010::is_StopOpticsAlignment_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT ||
		get_state()==Tango::RUNNING ||
		get_state()==Tango::DISABLE ||
		get_state()==Tango::ON)
	{
	/*----- PROTECTED REGION ID(IDS3010::StopOpticsAlignmentStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	IDS3010::StopOpticsAlignmentStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_ResetAllAxes_allowed()
 *	Description : Execution allowed for ResetAllAxes attribute
 */
//--------------------------------------------------------
bool IDS3010::is_ResetAllAxes_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT ||
		get_state()==Tango::DISABLE ||
		get_state()==Tango::ON)
	{
	/*----- PROTECTED REGION ID(IDS3010::ResetAllAxesStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	IDS3010::ResetAllAxesStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_ResetAxis_allowed()
 *	Description : Execution allowed for ResetAxis attribute
 */
//--------------------------------------------------------
bool IDS3010::is_ResetAxis_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT ||
		get_state()==Tango::DISABLE ||
		get_state()==Tango::ON)
	{
	/*----- PROTECTED REGION ID(IDS3010::ResetAxisStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	IDS3010::ResetAxisStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_EnablePilotLaser_allowed()
 *	Description : Execution allowed for EnablePilotLaser attribute
 */
//--------------------------------------------------------
bool IDS3010::is_EnablePilotLaser_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT ||
		get_state()==Tango::RUNNING ||
		get_state()==Tango::MOVING ||
		get_state()==Tango::DISABLE ||
		get_state()==Tango::ON)
	{
	/*----- PROTECTED REGION ID(IDS3010::EnablePilotLaserStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	IDS3010::EnablePilotLaserStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_DisablePilotLaser_allowed()
 *	Description : Execution allowed for DisablePilotLaser attribute
 */
//--------------------------------------------------------
bool IDS3010::is_DisablePilotLaser_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT ||
		get_state()==Tango::RUNNING ||
		get_state()==Tango::MOVING ||
		get_state()==Tango::DISABLE)
	{
	/*----- PROTECTED REGION ID(IDS3010::DisablePilotLaserStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	IDS3010::DisablePilotLaserStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : IDS3010::is_RebootSystem_allowed()
 *	Description : Execution allowed for RebootSystem attribute
 */
//--------------------------------------------------------
bool IDS3010::is_RebootSystem_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT)
	{
	/*----- PROTECTED REGION ID(IDS3010::RebootSystemStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	IDS3010::RebootSystemStateAllowed
		return false;
	}
	return true;
}


/*----- PROTECTED REGION ID(IDS3010::IDS3010StateAllowed.AdditionalMethods) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	IDS3010::IDS3010StateAllowed.AdditionalMethods

}	//	End of namespace
